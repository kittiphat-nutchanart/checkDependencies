const fs = require('fs');

function install(item, dependenciesGraph, installedItems) {
    // check if already install
    if (installedItems[item]) {
        console.log(' ' + item + ' is already installed.');
        return;
    }

    // recursively install if it depends others
    if (dependenciesGraph[item]) {
        Object.keys(dependenciesGraph[item]).forEach(dependency => {
            if (installedItems[dependency]) return;
            else install(dependency, dependenciesGraph, installedItems);
        });
    }


    console.log(' Installing ' + item);

    // mark the item as installed
    installedItems[item] = 1;
}

function remove(item, dependenciesGraph, installedItems, silent = false) {

    // check if installed
    if (!installedItems[item]) {
        if (!silent) console.log(' ' + item + ' is not installed.');
        return;
    }

    for (const installed of Object.keys(installedItems)) {
        if (installed === item) continue;

        if (dependenciesGraph[installed] && dependenciesGraph[installed][item]) {
            if (!silent) console.log(' ' + item + ' is still needed.');
            return;
        }
    }

    // {'a': ['b', 'c']}
    console.log(' Removing ' + item);

    // remove the item
    delete installedItems[item];

    // recursive remove if possible and make it slient
    if (dependenciesGraph[item]) {
        Object.keys(dependenciesGraph[item]).forEach(dep => {
            const silent = true;
            remove(dep, dependenciesGraph, installedItems, silent);
        });
    }

}


// you should examine the program from here
function systemDependencies(rawInput) {
    // e.g. {'TELNET': 1} means telnet is installed
    const installedItems = {};

    // e.g. {'c': ['a', 'b']} means c depends on a and b
    const dependenciesGraph = {};

    const instructionsArr = rawInput.split('\n').map(i => i.split(' '));
    // this looks like
    // [
    //     ['depend', 'a', 'b', 'c'],
    //     ['install', 'a'],
    //     ['reomve', 'a']
    // ]


    for (let i = 0; i < instructionsArr.length; i++) {

        // instruction e.g. ['depend', 'a', 'b', 'c']
        const ist = instructionsArr[i];

        // printing out the input e.g. DEPEND A B C
        console.log(ist.join(' ')); // required


        if (ist[0] === 'DEPEND') {
            const item = ist[1];

            const dependsOn = ist.slice(2);

            const dependsOnObj = {};

            dependsOn.forEach(i => {
                dependsOnObj[i] = 1;
            });

            dependenciesGraph[item] = dependsOnObj;

            continue;
        }

        if (ist[0] === 'INSTALL') {
            const item = ist[1];
            install(item, dependenciesGraph, installedItems);
            continue;
        }

        if (ist[0] === 'REMOVE') {
            const item = ist[1];
            remove(item, dependenciesGraph, installedItems);
            continue;
        }

        if (ist[0] === 'LIST') {

            // print out all installed deps
            Object.keys(installedItems).forEach(i => {
                console.log(' ' + i);
            });

            continue;
        }
    }

}

function main() {
    const inputPath = process.argv[2];
    const input = fs.readFileSync(inputPath);
    systemDependencies(input.toString().trim());
}

main();
