const tap = require('tap').mochaGlobals();
const { spawn } = require('child_process');
const assert = require('assert').strict;
const fs = require('fs');

// adjust these paths to fit your scenario
const scriptPath = 'index.js';
const inputPath = 'input.txt';
const outputPath = 'output.txt';

describe('testing your script', () => {

    it('should print the expected output', (done) => {

        const cli = spawn('node', [
            scriptPath,
            inputPath,
        ]);

        const chunks = [];

        cli.stdout.on('data', (chunk) => {

            chunks.push(chunk);

        });

        cli.stdout.on('end', () => {

            const output = Buffer.concat(chunks).toString();
            const expectedOutput = fs.readFileSync(outputPath).toString();
            assert.equal(output, expectedOutput);
            done();

        });

    });

});
