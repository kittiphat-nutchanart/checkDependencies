const fs = require('fs');

function doSth(rawInput) {
    // your code here
    // console.log(rawInput.split('\n'));
    const numbers = rawInput.split('\n').map(i => parseInt(i));

    let count = 0;
    numbers.forEach(i => {
        if (i % 2 === 1) count++;
    })

    console.log(count);
    //console.log(3);
    //console.log(2);
    //console.log(1);
}

function main() {
    const inputPath = process.argv[2];
    const input = fs.readFileSync(inputPath);
    doSth(input.toString().trim());
}

main();
