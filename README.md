# prerequesites
- node.js v12 LTS

# usage
        $ git clone https://gitlab.com/kittiphat-nutchanart/checkDependencies.git
        $ cd checkDependencies
        $ cd systemDependencies

        # to try other inputs change the content of input.txt
        # or replace input.txt with other e.g. input2.txt
        $ node index.js input.txt
